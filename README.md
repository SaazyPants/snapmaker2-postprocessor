# SNAPMAKER 2.0 :: FreeCAD RESOURCES
## BACKGROUND
Adds post processing support for the SnapMaker 2.0 to FreeCAD long with the SnapMaker 2.0 tool library. This README will not cover the installation of the latest version of FreeCAD. FreeCAD is expected to be installed prior to following the steps outlined below.

This project is a fork of a repository on GitHub with the intent to provide the following specifically for FreeCAD:

* Post Processor
* SnapMaker 2.0 Tooling Library (WIP)
* SnapMaker 2.0 Tooling Controller (WIP)
* Extended Tooling Library (WIP)

The original repository is provided in the references below. I do not maintain nor modify the post processing script itself and as such, plesae direct questions and/or feedback towards the original authors. Please advise if you'd like to see additional tools added to this repository or imrpove the default tooling controller settings, plesae let me know.

## COMPATIBILITY
This installation process was tested utilizing:

* FreeCAD Version 0.19 BUILD 24291 (GIT)
* Arch Linux (Manjaro & EndeavorOS)
* Windows 10
* Mac OS X

## INSTALLATION
In general, the *snapmaker_freecad_post.py* file included in this repository needs to be placed within FreeCAD's *Post* folder. Please follow your operating specific instructions below to locate this folder. Installation is as simple as copying the *snapmaker_freecad_post.py* file into this directory and restarting FreeCAD.

### LINUX
By default, the *Post* folder is located at the following location per the FreeCAD documentation:

```
/user/share/freecad/Mod/Path/Pathscripts/Post
```

FreeCAD comes with several *_post.py* files included within this directory by default. If the location above is empty (or doesn't exist), it's likely that your particular distribution has selected a non-default installation path. This is true with both Manjaro & EndeavorOS and likely others. To determine where the FreeCAD files are installed specific to your distribution, the following command can be run:

```
whereis freecad
```

This should return a list of locations in which your distribution stores the FreeCAD files. One of which should be where the binary is located and the other is where the support files are located. Both Manjaro & EndeavorOS locate their FreeCAD support files in a non-standard location as seen below:

```
/usr/lib/freecad/Mod/Path/Pathscripts/Post
```

### WINDOWS
By default, Windows 10 stores the *Post* folder at the following location:

```
C:\Users\$USER\AppData\Roaming\FreeCAD 0.19\Mod\Path\Pathscripts\Post
```

Replace the $USER variable above with your current username used to login to the Windows system. You may need to enable hidden files from the *View* menu in File Explorer in order to be able to see the *AppData* folder.

### MAC OS X
By default, Mac OS X stores this folder at the following location:

```
/Applications/FreeCAD.app/Contents/Resources/Mod/Path/Pathscripts/Post
```

In order to access this location, you'll need to right click on the "FreeCAD.app" file in the Applications directory and select the "Show Contents" option which will allow you to browser to the *Post* folder.

### MACRO FOLDER INSTALLATION (LINUX)
An alternative method to installing the *snapmaker_freecad_post.py* file to the *Post* folder is adding it to the *Macro* folder which for Linux distributions is located at the following directory within the user's home directory:

```
/home/$USER/.FreeCAD/Macro
```

If the *Macro* folder does not exist under *~/.FreeCAD*, it can be created and the *_post.py* file placed within it. FreeCAD will need to be rebooted if running.

## POST PROCESSING USAGE
Once installed, FreeCAD will need to be restarted assuming it's currently running. The Post Processor can be used by following the steps below in FreeCAD:

1. Create a new 'Job' from the *Path Workbench*
2. After selecting the appropriate bodies, from the *Output* tab, *snapmaker_freecad* should noe be available as a post processor
3. When post processing files using this processor, the files should be saved with the *.cnc* file extension in order for the SnapMaker 2.0 to recognize and load the file.

If you'd like the snapmaker_freecad post processor to be the default post processor for FreeCAD follow the steps below:

1. Got: Edit -> Preferences
2. Select the *Path* option from the left menu
3. Under *Job Preferences* select the *Post Processor* option.
4. Set the *Default Post Processing* drop down menu to *snapmaker_freecad*

It should be noted that this menu will also allow you to set the default file path that the post processor will place the finished *.cnc file into by setting the 'Default Path' variable.

## TOOL LIBRARY
### IMPORTING
WORK IN PROGRESS

## TOOL CONTROLLER
WORK IN PROGRESS

## LICENSE
GPL-1.0

## REFERENCE
### ORIGINAL REPOSITORY
* https://github.com/Snapmaker/snapmaker_cnc_post_process

### FREECAD DOCUMENTATION
* https://forum.freecadweb.org/viewforum.php?f=15
* https://wiki.freecad.org/Getting_started
